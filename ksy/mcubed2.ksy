---
meta:
  id: mcubed2
  endian: le

doc-ref: 'https://docs.google.com/spreadsheets/d/1kTfqIfh5AEzD56a7lo57BiqL4Rb87qpOVoilp4uiptA/'
doc: |
  :field mcubed2_callsign: ax25_frame.ax25_header.dest_callsign_raw.callsign_ror.callsign
  :field mcubed2_ssid_mask: ax25_frame.ax25_header.dest_ssid_raw.ssid_mask
  :field mcubed2_ssid: ax25_frame.ax25_header.dest_ssid_raw.ssid
  :field mcubed2_callsign: ax25_frame.ax25_header.src_callsign_raw.callsign_ror.callsign
  :field mcubed2_ssid_mask: ax25_frame.ax25_header.src_ssid_raw.ssid_mask
  :field mcubed2_ssid: ax25_frame.ax25_header.src_ssid_raw.ssid
  :field mcubed2_ctl: ax25_frame.ax25_header.ctl
  :field mcubed2_pid: ax25_frame.payload.pid
  :field mcubed2_ax25_info: ax25_frame.payload.ax25_info
  :field mcubed2_sync: ax25_frame.payload.ax25_info.header.sync
  :field mcubed2_id1: ax25_frame.payload.ax25_info.header.id1
  :field mcubed2_id2: ax25_frame.payload.ax25_info.header.id2
  :field mcubed2_flag: ax25_frame.payload.ax25_info.header.flag
  :field mcubed2_length: ax25_frame.payload.ax25_info.header.length
  :field mcubed2_chk: ax25_frame.payload.ax25_info.header.chk
  :field mcubed2_dat1: ax25_frame.payload.ax25_info.telemetry.dat1
  :field mcubed2_dat2: ax25_frame.payload.ax25_info.telemetry.dat2
  :field mcubed2_dat3: ax25_frame.payload.ax25_info.telemetry.dat3
  :field mcubed2_dat4: ax25_frame.payload.ax25_info.telemetry.dat4
  :field mcubed2_dat5: ax25_frame.payload.ax25_info.telemetry.dat5
  :field mcubed2_dat6: ax25_frame.payload.ax25_info.telemetry.dat6
  :field mcubed2_dat7: ax25_frame.payload.ax25_info.telemetry.dat7
  :field mcubed2_dat8: ax25_frame.payload.ax25_info.telemetry.dat8
  :field mcubed2_dat9: ax25_frame.payload.ax25_info.telemetry.dat9
  :field mcubed2_dat10: ax25_frame.payload.ax25_info.telemetry.dat10
  :field mcubed2_dat11: ax25_frame.payload.ax25_info.telemetry.dat11
  :field mcubed2_dat12: ax25_frame.payload.ax25_info.telemetry.dat12
  :field mcubed2_dat13: ax25_frame.payload.ax25_info.telemetry.dat13
  :field mcubed2_dat14: ax25_frame.payload.ax25_info.telemetry.dat14
  :field mcubed2_dat15: ax25_frame.payload.ax25_info.telemetry.dat15
  :field mcubed2_dat16: ax25_frame.payload.ax25_info.telemetry.dat16
  :field mcubed2_dat17: ax25_frame.payload.ax25_info.telemetry.dat17
  :field mcubed2_dat18: ax25_frame.payload.ax25_info.telemetry.dat18
  :field mcubed2_dat19: ax25_frame.payload.ax25_info.telemetry.dat19
  :field mcubed2_dat20: ax25_frame.payload.ax25_info.telemetry.dat20
  :field mcubed2_dat21: ax25_frame.payload.ax25_info.telemetry.dat21
  :field mcubed2_dat22: ax25_frame.payload.ax25_info.telemetry.dat22
  :field mcubed2_dat23: ax25_frame.payload.ax25_info.telemetry.dat23
  :field mcubed2_dat24: ax25_frame.payload.ax25_info.telemetry.dat24
  :field mcubed2_dat25: ax25_frame.payload.ax25_info.telemetry.dat25
  :field mcubed2_dat26: ax25_frame.payload.ax25_info.telemetry.dat26
  :field mcubed2_dat27: ax25_frame.payload.ax25_info.telemetry.dat27
  :field mcubed2_dat28: ax25_frame.payload.ax25_info.telemetry.dat28
  :field mcubed2_dat29: ax25_frame.payload.ax25_info.telemetry.dat29
  :field mcubed2_dat30: ax25_frame.payload.ax25_info.telemetry.dat30
  :field mcubed2_dat31: ax25_frame.payload.ax25_info.telemetry.dat31
  :field mcubed2_dat32: ax25_frame.payload.ax25_info.telemetry.dat32
  :field mcubed2_dat33: ax25_frame.payload.ax25_info.telemetry.dat33
  :field mcubed2_dat34: ax25_frame.payload.ax25_info.telemetry.dat34
  :field mcubed2_dat35: ax25_frame.payload.ax25_info.telemetry.dat35
  :field mcubed2_dat36: ax25_frame.payload.ax25_info.telemetry.dat36
  :field mcubed2_dat37: ax25_frame.payload.ax25_info.telemetry.dat37
  :field mcubed2_dat38: ax25_frame.payload.ax25_info.telemetry.dat38
  :field mcubed2_dat39: ax25_frame.payload.ax25_info.telemetry.dat39
  :field mcubed2_dat40: ax25_frame.payload.ax25_info.telemetry.dat40
  :field mcubed2_dat41: ax25_frame.payload.ax25_info.telemetry.dat41
  :field mcubed2_dat42: ax25_frame.payload.ax25_info.telemetry.dat42
  :field mcubed2_dat43: ax25_frame.payload.ax25_info.telemetry.dat43
  :field mcubed2_dat44: ax25_frame.payload.ax25_info.telemetry.dat44
  :field mcubed2_dat45: ax25_frame.payload.ax25_info.telemetry.dat45
  :field mcubed2_dat46: ax25_frame.payload.ax25_info.telemetry.dat46
  :field mcubed2_dat47: ax25_frame.payload.ax25_info.telemetry.dat47
  :field mcubed2_dat48: ax25_frame.payload.ax25_info.telemetry.dat48
  :field mcubed2_dat49: ax25_frame.payload.ax25_info.telemetry.dat49
  :field mcubed2_dat50: ax25_frame.payload.ax25_info.telemetry.dat50
  :field mcubed2_dat51: ax25_frame.payload.ax25_info.telemetry.dat51
  :field mcubed2_dat52: ax25_frame.payload.ax25_info.telemetry.dat52
  :field mcubed2_dat53: ax25_frame.payload.ax25_info.telemetry.dat53
  :field mcubed2_dat54: ax25_frame.payload.ax25_info.telemetry.dat54
  :field mcubed2_dat55: ax25_frame.payload.ax25_info.telemetry.dat55
  :field mcubed2_dat56: ax25_frame.payload.ax25_info.telemetry.dat56
  :field mcubed2_dat57: ax25_frame.payload.ax25_info.telemetry.dat57
  :field mcubed2_dat58: ax25_frame.payload.ax25_info.telemetry.dat58
  :field mcubed2_dat59: ax25_frame.payload.ax25_info.telemetry.dat59
  :field mcubed2_dat60: ax25_frame.payload.ax25_info.telemetry.dat60
  :field mcubed2_dat61: ax25_frame.payload.ax25_info.telemetry.dat61
  :field mcubed2_dat62: ax25_frame.payload.ax25_info.telemetry.dat62
  :field mcubed2_dat63: ax25_frame.payload.ax25_info.telemetry.dat63
  :field mcubed2_dat64: ax25_frame.payload.ax25_info.telemetry.dat64
  :field mcubed2_dat65: ax25_frame.payload.ax25_info.telemetry.dat65
  :field mcubed2_dat66: ax25_frame.payload.ax25_info.telemetry.dat66
  :field mcubed2_dat67: ax25_frame.payload.ax25_info.telemetry.dat67
  :field mcubed2_dat68: ax25_frame.payload.ax25_info.telemetry.dat68
  :field mcubed2_dat69: ax25_frame.payload.ax25_info.telemetry.dat69
  :field mcubed2_dat70: ax25_frame.payload.ax25_info.telemetry.dat70
  :field mcubed2_dat71: ax25_frame.payload.ax25_info.telemetry.dat71
  :field mcubed2_dat72: ax25_frame.payload.ax25_info.telemetry.dat72
  :field mcubed2_dat73: ax25_frame.payload.ax25_info.telemetry.dat73
  :field mcubed2_dat74: ax25_frame.payload.ax25_info.telemetry.dat74
  :field mcubed2_dat75: ax25_frame.payload.ax25_info.telemetry.dat75
  :field mcubed2_dat76: ax25_frame.payload.ax25_info.telemetry.dat76
  :field mcubed2_dat77: ax25_frame.payload.ax25_info.telemetry.dat77
  :field mcubed2_dat78: ax25_frame.payload.ax25_info.telemetry.dat78
  :field mcubed2_dat79: ax25_frame.payload.ax25_info.telemetry.dat79
  :field mcubed2_dat80: ax25_frame.payload.ax25_info.telemetry.dat80
  :field mcubed2_dat81: ax25_frame.payload.ax25_info.telemetry.dat81
  :field mcubed2_dat82: ax25_frame.payload.ax25_info.telemetry.dat82
  :field mcubed2_dat83: ax25_frame.payload.ax25_info.telemetry.dat83
  :field mcubed2_dat84: ax25_frame.payload.ax25_info.telemetry.dat84
  :field mcubed2_dat85: ax25_frame.payload.ax25_info.telemetry.dat85
  :field mcubed2_dat86: ax25_frame.payload.ax25_info.telemetry.dat86
  :field mcubed2_dat87: ax25_frame.payload.ax25_info.telemetry.dat87
  :field mcubed2_dat88: ax25_frame.payload.ax25_info.telemetry.dat88
  :field mcubed2_dat89: ax25_frame.payload.ax25_info.telemetry.dat89
  :field mcubed2_dat90: ax25_frame.payload.ax25_info.telemetry.dat90
  :field mcubed2_dat91: ax25_frame.payload.ax25_info.telemetry.dat91
  :field mcubed2_dat92: ax25_frame.payload.ax25_info.telemetry.dat92
  :field mcubed2_dat93: ax25_frame.payload.ax25_info.telemetry.dat93
  :field mcubed2_dat94: ax25_frame.payload.ax25_info.telemetry.dat94
  :field mcubed2_dat95: ax25_frame.payload.ax25_info.telemetry.dat95
  :field mcubed2_dat96: ax25_frame.payload.ax25_info.telemetry.dat96
  :field mcubed2_dat97: ax25_frame.payload.ax25_info.telemetry.dat97
  :field mcubed2_dat98: ax25_frame.payload.ax25_info.telemetry.dat98
  :field mcubed2_dat99: ax25_frame.payload.ax25_info.telemetry.dat99
  :field mcubed2_dat100: ax25_frame.payload.ax25_info.telemetry.dat100
  :field mcubed2_dat101: ax25_frame.payload.ax25_info.telemetry.dat101
  :field mcubed2_dat102: ax25_frame.payload.ax25_info.telemetry.dat102
  :field mcubed2_dat103: ax25_frame.payload.ax25_info.telemetry.dat103
  :field mcubed2_dat104: ax25_frame.payload.ax25_info.telemetry.dat104
  :field mcubed2_dat105: ax25_frame.payload.ax25_info.telemetry.dat105
  :field mcubed2_dat106: ax25_frame.payload.ax25_info.telemetry.dat106
  :field mcubed2_dat107: ax25_frame.payload.ax25_info.telemetry.dat107
  :field mcubed2_dat108: ax25_frame.payload.ax25_info.telemetry.dat108
  :field mcubed2_dat109: ax25_frame.payload.ax25_info.telemetry.dat109
  :field mcubed2_dat110: ax25_frame.payload.ax25_info.telemetry.dat110
  :field mcubed2_dat111: ax25_frame.payload.ax25_info.telemetry.dat111
  :field mcubed2_chk: ax25_frame.payload.ax25_info.footer.chk
  :field mcubed2_unknown: ax25_frame.payload.ax25_info.footer.unknown

seq:
  - id: ax25_frame
    type: ax25_frame
    doc-ref: 'https://www.tapr.org/pub_ax25.html'

types:
  ax25_frame:
    seq:
      - id: ax25_header
        type: ax25_header
      - id: payload
        type:
          switch-on: ax25_header.ctl & 0x13
          cases:
            0x03: ui_frame
            0x13: ui_frame
            0x00: i_frame
            0x02: i_frame
            0x10: i_frame
            0x12: i_frame
            # 0x11: s_frame

  ax25_header:
    seq:
      - id: dest_callsign_raw
        type: callsign_raw
      - id: dest_ssid_raw
        type: ssid_mask
      - id: src_callsign_raw
        type: callsign_raw
      - id: src_ssid_raw
        type: ssid_mask
      - id: ctl
        type: u1

  callsign_raw:
    seq:
      - id: callsign_ror
        process: ror(1)
        size: 6
        type: callsign

  callsign:
    seq:
      - id: callsign
        type: str
        encoding: ASCII
        size: 6

  ssid_mask:
    seq:
      - id: ssid_mask
        type: u1
    instances:
      ssid:
        value: (ssid_mask & 0x0f) >> 1

  i_frame:
    seq:
      - id: pid
        type: u1
      - id: ax25_info
        size-eos: true

  ui_frame:
    seq:
      - id: pid
        type: u1
      - id: ax25_info
        type: mcubed2_frame
        size-eos: true

  mcubed2_frame:
    seq:
      - id: header
        type: m_cubed_2_header
      - id: telemetry
        type: m_cubed_2_telemetry
      - id: footer
        type: m_cubed_2_footer

  m_cubed_2_header:
    seq:
      - id: sync
        type: u2
        doc: "Sync Char"
      - id: id1
        type: u2
        doc: "Primary ID"
      - id: id2
        type: u2
        doc: "Secondary ID"
      - id: flag
        type: u1
        doc: "Flag"
      - id: length
        type: u2
        doc: "Length"
      - id: chk
        type: u2
        doc: "Header Checksum"

  m_cubed_2_telemetry:
    seq:
      - id: dat1
        type: u4
        doc: "RTC Unix Time"
      - id: dat2
        type: u2
        doc: "NumResets"
      - id: dat3
        type: u2
        doc: "avgNumActiveTasks1"
      - id: dat4
        type: u2
        doc: "avgNumActiveTasks5"
      - id: dat5
        type: u2
        doc: "avgNumActiveTasks15"
      - id: dat6
        type: u2
        doc: "curNumRunnableTasks"
      - id: dat7
        type: u2
        doc: "totNumProcesses"
      - id: dat8
        type: u2
        doc: "lastProcessPID"
      - id: dat9
        type: u2
        doc: "totMem"
      - id: dat10
        type: u2
        doc: "freeMem"
      - id: dat11
        type: u2
        doc: "Lithium Op Count"
      - id: dat12
        type: u2
        doc: "Lithium MSP430 Temp"
      - id: dat13
        type: u1
        doc: "Lithium RSSI"
      - id: dat14
        type: u4
        doc: "Lithium #RX"
      - id: dat15
        type: u4
        doc: "Lithium #TX"
      - id: dat16
        type: u2
        doc: "Ch. 1 Module Output Current"
      - id: dat17
        type: u2
        doc: "Ch. 1A Current"
      - id: dat18
        type: u2
        doc: "Ch. 1 Module Temp"
      - id: dat19
        type: u2
        doc: "Ch. 1A Voltage"
      - id: dat20
        type: u2
        doc: "Ch. 2 Module Output Current"
      - id: dat21
        type: u2
        doc: "Ch. 2A Current"
      - id: dat22
        type: u2
        doc: "Ch. 2 Module Temp"
      - id: dat23
        type: u2
        doc: "Ch. 2A Voltage"
      - id: dat24
        type: u2
        doc: "Ch. 3 Module Output Current"
      - id: dat25
        type: u2
        doc: "Ch. 3A Current"
      - id: dat26
        type: u2
        doc: "Ch. 3 Module Temp"
      - id: dat27
        type: u2
        doc: "Ch. 3A Voltage"
      - id: dat28
        type: u2
        doc: "Ch. 4 Module Output Current"
      - id: dat29
        type: u2
        doc: "Ch. 4A Current"
      - id: dat30
        type: u2
        doc: "Ch. 4 Module Temp"
      - id: dat31
        type: u2
        doc: "Ch. 4A Voltage"
      - id: dat32
        type: u2
        doc: "Ch. 5 Module Output Current"
      - id: dat33
        type: u2
        doc: "Ch. 5A Current"
      - id: dat34
        type: u2
        doc: "Ch. 5 Module Temp"
      - id: dat35
        type: u2
        doc: "Ch. 5A Voltage"
      - id: dat36
        type: u2
        doc: "Ch. 6 Module Output Current"
      - id: dat37
        type: u2
        doc: "Ch. 6A Current"
      - id: dat38
        type: u2
        doc: "Ch. 6 Module Temp"
      - id: dat39
        type: u2
        doc: "Ch. 6A Voltage"
      - id: dat40
        type: u2
        doc: "Regulator 5V input Current"
      - id: dat41
        type: u2
        doc: "Regulator 3.3V input Current"
      - id: dat42
        type: u2
        doc: "BB_T_Output"
      - id: dat43
        type: u2
        doc: "Bus VBatt Current"
      - id: dat44
        type: u2
        doc: "Bus 5V Current"
      - id: dat45
        type: u2
        doc: "Bus 3.3V Voltage"
      - id: dat46
        type: u2
        doc: "Bus 3.3V Current"
      - id: dat47
        type: u2
        doc: "Bus VBatt Voltage"
      - id: dat48
        type: u2
        doc: "Bus 5V Voltage"
      - id: dat49
        type: u2
        doc: "EPS 5v Current"
      - id: dat50
        type: u2
        doc: "EPS 5V Voltage"
      - id: dat51
        type: u2
        doc: "EPS 3.3V Current"
      - id: dat52
        type: s2
        doc: "BB_EPS_batt_I"
      - id: dat53
        type: u2
        doc: "EPS 3.3V Voltage"
      - id: dat54
        type: u2
        doc: "Battery Temperature"
      - id: dat55
        type: b8
        doc: "IOE1 IOE STATE"
      - id: dat56
        type: b8
        doc: "IOE1 IOE MODE"
      - id: dat57
        type: b8
        doc: "FCPU1 IOE STATE"
      - id: dat58
        type: b8
        doc: "FCPU1 IOE MODE"
      - id: dat59
        type: b8
        doc: "ADCS1 IOE STATE"
      - id: dat60
        type: b8
        doc: "ADCS1 IOE MODE"
      - id: dat61
        type: b8
        doc: "EPS IOE STATE"
      - id: dat62
        type: b8
        doc: "EPS IOE MODE"
      - id: dat63
        type: b8
        doc: "MZINT1 IOE STATE"
      - id: dat64
        type: b8
        doc: "MZINT1 IOE MODE"
      - id: dat65
        type: u2
        doc: "ACB TEMP 0"
      - id: dat66
        type: u2
        doc: "ACB TEMP 1"
      - id: dat67
        type: u2
        doc: "ACB VBATT Voltage"
      - id: dat68
        type: u2
        doc: "ACB VBATT Current"
      - id: dat69
        type: u2
        doc: "ACB 5V Voltage"
      - id: dat70
        type: u2
        doc: "ACB 5V Current"
      - id: dat71
        type: u2
        doc: "ACB 3V3 Voltage"
      - id: dat72
        type: u2
        doc: "ACB 3V3 Current"
      - id: dat73
        type: u2
        doc: "FCPU TEMP 0"
      - id: dat74
        type: u2
        doc: "FCPU TEMP 1"
      - id: dat75
        type: u2
        doc: "LI 3V3 Voltage"
      - id: dat76
        type: u2
        doc: "FCPU 3V3 Current"
      - id: dat77
        type: u2
        doc: "LI 3V3 Current"
      - id: dat78
        type: u2
        doc: "FCPU 3V3 Voltage"
      - id: dat79
        type: u2
        doc: "LI VBATT Voltage"
      - id: dat80
        type: u2
        doc: "LI VBATT Current"
      - id: dat81
        type: s2
        doc: "-Z MAG X"
      - id: dat82
        type: s2
        doc: "-Z MAG Y"
      - id: dat83
        type: s2
        doc: "-Z MAG Z"
      - id: dat84
        type: u2
        doc: "-Z TEMP 0"
      - id: dat85
        type: u2
        doc: "-Z TEMP 1"
      - id: dat86
        type: u2
        doc: "+Z TEMP 0"
      - id: dat87
        type: u2
        doc: "+Z TEMP 1"
      - id: dat88
        type: u2
        doc: "-X TEMP 0"
      - id: dat89
        type: u2
        doc: "-X TEMP 1"
      - id: dat90
        type: u2
        doc: "+X TEMP 0"
      - id: dat91
        type: u2
        doc: "+X TEMP 1"
      - id: dat92
        type: u2
        doc: "-Y TEMP 0"
      - id: dat93
        type: u2
        doc: "-Y TEMP 1"
      - id: dat94
        type: u2
        doc: "+Y TEMP 0"
      - id: dat95
        type: u2
        doc: "+Y TEMP 1"
      - id: dat96
        type: u2
        doc: "-Z Photodiode"
      - id: dat97
        type: u2
        doc: "+Z Photodiode"
      - id: dat98
        type: u2
        doc: "-X Photodiode"
      - id: dat99
        type: u2
        doc: "+X Photodiode"
      - id: dat100
        type: u2
        doc: "-Y Photodiode"
      - id: dat101
        type: u2
        doc: "+Y Photodiode"
      - id: dat102
        type: s2
        doc: "+X MAG X"
      - id: dat103
        type: s2
        doc: "+X MAG Y"
      - id: dat104
        type: s2
        doc: "+X MAG Z"
      - id: dat105
        type: u4
        doc: "Cove experiment md5sum p1"
      - id: dat106
        type: u4
        doc: "Cove experiment md5sum p2"
      - id: dat107
        type: u4
        doc: "Cove experiment md5sum p3"
      - id: dat108
        type: u4
        doc: "Cove experiment md5sum p4"
      - id: dat109
        type: u1
        doc: "Cove status data"
      - id: dat110
        type: u2
        doc: "Cove Num Successes"
      - id: dat111
        type: u2
        doc: "Cove Num Failures"

  m_cubed_2_footer:
    seq:
      - id: chk
        type: u2
        doc: "Checksum"
      - id: unknown
        type: u4
