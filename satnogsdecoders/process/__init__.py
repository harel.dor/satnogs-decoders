"""
SatNOGS Processor subpackage initialization
"""
from __future__ import absolute_import, division, print_function

from .elfin_pp import ElfinPp

__all__ = ['ElfinPp']
